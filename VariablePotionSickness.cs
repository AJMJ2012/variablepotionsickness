﻿using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System;
using Terraria.ID;
using Terraria.IO;
using Terraria.ModLoader;
using Terraria;

namespace VariablePotionSickness {
	public enum Requests : byte {
		RequestData,
		ReceiveConfig
	}

	public class VariablePotionSickness : Mod {
		public VariablePotionSickness() {
			Properties = new ModProperties {
				Autoload = true
			};
		}

		public override void Unload() {
			Item.potionDelay = 3600;
			Item.restorationDelay = 3000;
			Player.manaSickTime = 300;
			Player.manaSickTimeMax = 900;
			Player.manaSickLessDmg = 0.25f;
		}

		public static bool TooltipsPlusLoaded = false;
		bool LoadedFKTModSettings = false;
		public override void Load() {
			Config.ReadConfig();
			TooltipsPlusLoaded = ModLoader.GetMod("TooltipsPlus") != null;
			if (Main.netMode == 2) { return; }
			LoadedFKTModSettings = ModLoader.GetMod("FKTModSettings") != null;
			if (LoadedFKTModSettings) {
				try { LoadModSettings(); }
				catch (Exception e) {
					DALib.Logger.ErrorLog("Unable to Load Mod Settings", Config.modPrefix);
					DALib.Logger.ErrorLog(e, Config.modPrefix);
				}
			}
		}

		public override void PostUpdateInput() {
			if (LoadedFKTModSettings && !Main.gameMenu && Main.netMode != 2) {
				if (DALib.DALib.tick % 60 == 0) {
					try {
						List<Type> Types = new List<Type>{ typeof(Config.Global) };
						if (Main.netMode != 2) { Types.Add(typeof(Config.Client)); }
						if (Main.netMode != 1) { Types.Add(typeof(Config.Server)); }
						string OldConfig = null;
						foreach (Type type in Types) { OldConfig += JsonConvert.SerializeObject((type.GetFields(BindingFlags.Static | BindingFlags.Public).Select(field => field.GetValue(type)))); }
						UpdateModSettings();
						string NewConfig = null;
						foreach (Type type in Types) { NewConfig += JsonConvert.SerializeObject((type.GetFields(BindingFlags.Static | BindingFlags.Public).Select(field => field.GetValue(type)))); }
						if (OldConfig != NewConfig) { Config.SaveConfig(); }
					}
					catch (Exception e) {
						DALib.Logger.ErrorLog("Unable to compare config data", Config.modPrefix);
						DALib.Logger.ErrorLog(e, Config.modPrefix);
					}
				}
			}
		}

		private void LoadModSettings() {
			FKTModSettings.ModSetting setting = FKTModSettings.ModSettingsAPI.CreateModSettingConfig(this);
			setting.EnableAutoConfig();
			setting.AddComment("[c/"+Colors.RarityRed.Hex3()+":Health Settings]", 1f);
			setting.AddBool("EnableLife", "Enabled", true);
			setting.AddBool("UseLifeRatio", "Use ratio method", true);
			setting.AddInt("BaseLifeAmount", "Base amount", 0, 2500, true);
			setting.AddInt("BaseLifeSickTime", "Base sick time (Per HP)", 0, 300, true);
			setting.AddInt("AdditionalLifeSeconds", "Additional sick time", -300, 300, true);
			setting.AddInt("LifeSickSecondsMax", "Maximum sickness time", 0, 300, true);

			setting.AddComment("", 0f);

			setting.AddComment("[c/"+Colors.RarityBlue.Hex3()+":Mana Settings]", 1f);
			setting.AddBool("EnableMana", "Enabled", true);
			setting.AddBool("UseManaRatio", "Use ratio method", true);
			setting.AddInt("BaseManaAmount", "Base amount", 0, 2500, true);
			setting.AddInt("BaseManaSickTime", "Base sick time (Per MP)", 0, 300, true);
			setting.AddInt("AdditionalManaSeconds", "Additional sick time", -300, 300, true);
			setting.AddInt("ManaSickSecondsMax", "Maximum sickness time", 0, 300, true);
			setting.AddInt("ManaSickPowerReduction", "Sickness power reduction %", 0, 100, true);
			setting.AddBool("PhilosophersReduceMana", "Philosophers reduces mana sickness", true);
		}

		private void UpdateModSettings() {
			FKTModSettings.ModSetting setting;
			if (FKTModSettings.ModSettingsAPI.TryGetModSetting(this, out setting))  {
				setting.Get("AdditionalLifeSeconds", ref Config.Server.AdditionalLifeSeconds);
				setting.Get("AdditionalManaSeconds", ref Config.Server.AdditionalManaSeconds);
				setting.Get("BaseLifeAmount", ref Config.Server.BaseLifeAmount);
				setting.Get("BaseLifeSickTime", ref Config.Server.BaseLifeSickTime);
				setting.Get("BaseManaAmount", ref Config.Server.BaseManaAmount);
				setting.Get("BaseManaSickTime", ref Config.Server.BaseManaSickTime);
				setting.Get("EnableLife", ref Config.Server.EnableLife);
				setting.Get("EnableMana", ref Config.Server.EnableMana);
				setting.Get("LifeSickSecondsMax", ref Config.Server.LifeSickSecondsMax);
				setting.Get("ManaSickPowerReduction", ref Config.Server.ManaSickPowerReduction);
				setting.Get("ManaSickSecondsMax", ref Config.Server.ManaSickSecondsMax);
				setting.Get("PhilosophersReduceMana", ref Config.Server.PhilosophersReduceMana);
				setting.Get("UseLifeRatio", ref Config.Server.UseLifeRatio);
				setting.Get("UseManaRatio", ref Config.Server.UseManaRatio);
			}
		}

		public override void HandlePacket(BinaryReader reader, int whoAmI) {
			if (reader.ReadString() != Config.modPrefix) { return; }
			Requests request = (Requests)reader.ReadByte();
			if (Main.netMode == 2) {
				if (request == Requests.RequestData) {
					Config.SendConfig(this, whoAmI);
				}
			}
			else {
				if (request == Requests.ReceiveConfig) {
					DALib.Logger.DebugLog("Received Config from Server", Config.modPrefix);
					Config.Server.AdditionalLifeSeconds = reader.ReadInt32();
					Config.Server.AdditionalManaSeconds = reader.ReadInt32();
					Config.Server.BaseLifeAmount = reader.ReadInt32();
					Config.Server.BaseLifeSickTime = reader.ReadInt32();
					Config.Server.BaseManaAmount = reader.ReadInt32();
					Config.Server.BaseManaSickTime = reader.ReadInt32();
					Config.Server.EnableLife = reader.ReadBoolean();
					Config.Server.EnableMana = reader.ReadBoolean();
					Config.Server.LifeSickSecondsMax = reader.ReadInt32();
					Config.Server.ManaSickPowerReduction = reader.ReadInt32();
					Config.Server.ManaSickSecondsMax = reader.ReadInt32();
					Config.Server.PhilosophersReduceMana = reader.ReadBoolean();
					Config.Server.UseLifeRatio = reader.ReadBoolean();
					Config.Server.UseManaRatio = reader.ReadBoolean();
				}
			}
		}
	}

	public static class Config {
		public static class Global {}
		public static class Client {}
		public static class Server {
			public static bool EnableLife = true;
			public static bool UseLifeRatio = false;
			public static int BaseLifeAmount = 150;
			public static int BaseLifeSickTime = 48;
			public static int AdditionalLifeSeconds = 12;
			public static int LifeSickSecondsMax = 120;

			public static bool EnableMana = true;
			public static bool UseManaRatio = false;
			public static int BaseManaAmount = 200;
			public static int BaseManaSickTime = 8;
			public static int AdditionalManaSeconds = 2;
			public static int ManaSickSecondsMax = 20;
			public static int ManaSickPowerReduction = 25;
			public static bool PhilosophersReduceMana = true;
		}

		public static string modName = "VariablePotionSickness";
		public static string modPrefix = "VPS";
		private static Preferences Configuration = new Preferences(Path.Combine(Main.SavePath, "Mod Configs/" + modName + ".json"));

		public static void ReadConfig() {
			if(Configuration.Load()) {
				if (Main.netMode != 1) {
					Configuration.Get("AdditionalLifeSeconds", ref Server.AdditionalLifeSeconds);
					Configuration.Get("AdditionalManaSeconds", ref Server.AdditionalManaSeconds);
					Configuration.Get("BaseLifeAmount", ref Server.BaseLifeAmount);
					Configuration.Get("BaseLifeSickTime", ref Server.BaseLifeSickTime);
					Configuration.Get("BaseManaAmount", ref Server.BaseManaAmount);
					Configuration.Get("BaseManaSickTime", ref Server.BaseManaSickTime);
					Configuration.Get("EnableLife", ref Server.EnableLife);
					Configuration.Get("EnableMana", ref Server.EnableMana);
					Configuration.Get("LifeSickSecondsMax", ref Server.LifeSickSecondsMax);
					Configuration.Get("ManaSickPowerReduction", ref Server.ManaSickPowerReduction);
					Configuration.Get("ManaSickSecondsMax", ref Server.ManaSickSecondsMax);
					Configuration.Get("PhilosophersReduceMana", ref Server.PhilosophersReduceMana);
					Configuration.Get("UseLifeRatio", ref Server.UseLifeRatio);
					Configuration.Get("UseManaRatio", ref Server.UseManaRatio);
				}
				DALib.Logger.DebugLog("Config Loaded", Config.modPrefix);
			}
			else {
				DALib.Logger.DebugLog("Creating Config", Config.modPrefix);
			}
			SaveConfig();
		}

		public static void ClampConfig() {
			Server.AdditionalLifeSeconds = (int)MathHelper.Clamp(Server.AdditionalLifeSeconds, -300, 300);
			Server.AdditionalManaSeconds = (int)MathHelper.Clamp(Server.AdditionalManaSeconds, -300, 300);
			Server.BaseLifeAmount = (int)MathHelper.Clamp(Server.BaseLifeAmount, 0, 2500);
			Server.BaseLifeSickTime = (int)MathHelper.Clamp(Server.BaseLifeSickTime, 0, 300);
			Server.BaseManaAmount = (int)MathHelper.Clamp(Server.BaseManaAmount, 0, 2500);
			Server.BaseManaSickTime = (int)MathHelper.Clamp(Server.BaseManaSickTime, 0, 300);
			Server.LifeSickSecondsMax = (int)MathHelper.Clamp(Server.LifeSickSecondsMax, 0, 300);
			Server.ManaSickPowerReduction = (int)MathHelper.Clamp(Server.ManaSickPowerReduction, 0, 100);
			Server.ManaSickSecondsMax = (int)MathHelper.Clamp(Server.ManaSickSecondsMax, 0, 300);
		}

		public static void SaveConfig() {
			ClampConfig();
			if (Main.netMode != 1) { Configuration.Clear(); }
			if (Main.netMode != 1) {
				Configuration.Put("EnableLife", Server.EnableLife);
				Configuration.Put("UseLifeRatio", Server.UseLifeRatio);
				Configuration.Put("BaseLifeAmount", Server.BaseLifeAmount);
				Configuration.Put("BaseLifeSickTime", Server.BaseLifeSickTime);
				Configuration.Put("AdditionalLifeSeconds", Server.AdditionalLifeSeconds);
				Configuration.Put("LifeSickSecondsMax", Server.LifeSickSecondsMax);
				
				Configuration.Put("EnableMana", Server.EnableMana);
				Configuration.Put("UseManaRatio", Server.UseManaRatio);
				Configuration.Put("BaseManaAmount", Server.BaseManaAmount);
				Configuration.Put("BaseManaSickTime", Server.BaseManaSickTime);
				Configuration.Put("AdditionalManaSeconds", Server.AdditionalManaSeconds);
				Configuration.Put("ManaSickSecondsMax", Server.ManaSickSecondsMax);
				Configuration.Put("ManaSickPowerReduction", Server.ManaSickPowerReduction);
				Configuration.Put("PhilosophersReduceMana", Server.PhilosophersReduceMana);
			}
			Configuration.Save();
			DALib.Logger.DebugLog("Config Saved", Config.modPrefix);
		}

		public static void SendConfig(Mod mod, int whoAmI = -1) {
			ModPacket netMessage = mod.GetPacket();
			netMessage.Write(Config.modPrefix);
			netMessage.Write((byte)Requests.ReceiveConfig);
			netMessage.Write(Config.Server.AdditionalLifeSeconds);
			netMessage.Write(Config.Server.AdditionalManaSeconds);
			netMessage.Write(Config.Server.BaseLifeAmount);
			netMessage.Write(Config.Server.BaseLifeSickTime);
			netMessage.Write(Config.Server.BaseManaAmount);
			netMessage.Write(Config.Server.BaseManaSickTime);
			netMessage.Write(Config.Server.EnableLife);
			netMessage.Write(Config.Server.EnableMana);
			netMessage.Write(Config.Server.LifeSickSecondsMax);
			netMessage.Write(Config.Server.ManaSickPowerReduction);
			netMessage.Write(Config.Server.ManaSickSecondsMax);
			netMessage.Write(Config.Server.PhilosophersReduceMana);
			netMessage.Write(Config.Server.UseLifeRatio);
			netMessage.Write(Config.Server.UseManaRatio);
			if (whoAmI >= 0) { netMessage.Send(whoAmI); } else { netMessage.Send(); }
			DALib.Logger.DebugLog("Config sent to " + (whoAmI >= 0 ? Main.player[whoAmI].name : "all players"), Config.modPrefix);
		}
	}

	public class Commands : ModCommand {
		public override CommandType Type {
			get { return CommandType.Chat | CommandType.Server | CommandType.Console; }
		}
		public override string Command {
			get { return Config.modPrefix.ToLower(); }
		}
		public override string Description {
			get { return Config.modName; }
		}
		public override string Usage {
			get { return Command + " reload"; }
		}
		public override void Action(CommandCaller caller, string input, string[] args) {
			switch (args[0].ToLower()) {
				case "reload":
					DALib.Logger.Log("Config Reloaded", Config.modPrefix);
					Config.ReadConfig();
					if (Main.netMode == 2) {
						Config.SendConfig(mod);
					}
					return;
			}
			Config.SaveConfig();
		}
	}
}
