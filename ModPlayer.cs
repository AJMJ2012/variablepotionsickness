﻿using System;
using System.IO;
using Terraria;
using Terraria.IO;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;

namespace VariablePotionSickness {
	public class MPlayer : ModPlayer {
		public override void OnEnterWorld(Player player) {
			if (player == null) { return; }
			if (player.whoAmI == this.player.whoAmI) {
				if (Main.netMode == 1) {
					DALib.Logger.DebugLog("Requesting Config from Server", Config.modPrefix);
					ModPacket netMessage = mod.GetPacket();
					netMessage.Write(Config.modPrefix);
					netMessage.Write((byte)Requests.RequestData);
					netMessage.Write((int)player.whoAmI);
					netMessage.Send();
				}
			}
		}

		public override void PreUpdate() {
			if (Config.Server.EnableLife) {
				Item.potionDelay = 0; //Config.Server.BaseLifeSickTime * 60;
				Item.restorationDelay = 0; //Math.Abs(Config.Server.BaseLifeSickTime - Config.Server.BaseManaSickTime) * 60;
			}
			else {
				Item.potionDelay = 3600;
				Item.restorationDelay = 3000;
			}
			if (Config.Server.EnableMana) {
				Player.manaSickTime = 0; //Config.Server.BaseManaSickTime * 60;
				Player.manaSickTimeMax = (int)(Config.Server.ManaSickSecondsMax * (player.pStone && Config.Server.PhilosophersReduceMana ? 0.75f : 1f)) * 60;
				Player.manaSickLessDmg = Config.Server.ManaSickPowerReduction / 100f;
			}
			else {
				Player.manaSickTime = 300;
				Player.manaSickTimeMax = 900;
				Player.manaSickLessDmg = 0.25f;
			}
		}

		public override void PostUpdate() {
			if (Config.Server.EnableMana) {
				int sicknessTime = 0;
				for (int i = 0; i < player.buffType.Length; i++) {
					if (player.buffType[i] == 94) {
						sicknessTime = player.buffTime[i];
						break;
					}
				}
				player.manaSickReduction = Player.manaSickLessDmg * (sicknessTime / (float)Player.manaSickTimeMax);
			}
		}
	}
}
