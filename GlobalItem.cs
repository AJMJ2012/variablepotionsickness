﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria;
using System.Text.RegularExpressions;

namespace VariablePotionSickness {
	public class GItem : GlobalItem {
		public static float GetLifeSickness(Item item, Player player, bool cap = true) {
			float delay = 0;
			if (Config.Server.UseLifeRatio) {
				delay = (((float)item.healLife / (float)player.statLifeMax2) * Config.Server.BaseLifeSickTime);
			}
			else {
				delay = ((float)item.healLife / (float)((float)Config.Server.BaseLifeAmount / (float)(Config.Server.BaseLifeSickTime)));
			}
			if (item.type == 227 || player.pStone) {
				delay -= delay / 4f;
			}
			delay += Config.Server.AdditionalLifeSeconds;
			return (float)Math.Min(delay, Config.Server.LifeSickSecondsMax * (item.type == 227 || player.pStone ? 0.75f : 1f)) * 60;
		}
		
		public static float GetManaSickness(Item item, Player player, bool cap = true) {
			float delay = 0;
			if (Config.Server.UseManaRatio) {
				delay = (((float)item.healMana / (float)player.statManaMax2) * Config.Server.BaseManaSickTime);
			}
			else {
				delay = ((float)item.healMana / (float)((float)Config.Server.BaseManaAmount / (float)(Config.Server.BaseManaSickTime)));
			}
			if (item.type == 227 || (player.pStone && Config.Server.PhilosophersReduceMana)) {
				delay -= delay / 4f;
			}
			delay += Config.Server.AdditionalManaSeconds;
			return (float)Math.Min(delay, Config.Server.ManaSickSecondsMax * (item.type == 227 || (player.pStone && Config.Server.PhilosophersReduceMana) ? 0.75f : 1f)) * 60;
		}
		
		
		public override bool UseItem(Item item, Player player) {
			if (Config.Server.EnableLife && item.healLife > 0 && player.statLifeMax2 > 0) {
				float delay = GetLifeSickness(item, player);
				DALib.Logger.DebugLog("Adding " + (delay / 60f) + " seconds of potion sickness", Config.modPrefix);
				player.AddBuff(21, (int) delay, false);
			}
			if (Config.Server.EnableMana && item.healMana > 0 && player.statManaMax2 > 0) {
				float delay = GetManaSickness(item, player);
				DALib.Logger.DebugLog("Adding " + (delay / 60f) + " seconds of mana sickness", Config.modPrefix);
				player.AddBuff(94, (int) delay, true);
			}
			return false;
		}
		
		public override void ModifyTooltips(Item item, List<TooltipLine> tooltips) {
			Player player = Main.player[Main.myPlayer];
			if (!VariablePotionSickness.TooltipsPlusLoaded || item.type == 0 || (item.healLife <= 0 && item.healMana <= 0)) { return; }
			for (int i=0; i < tooltips.Count; i++) {
				if (tooltips[i].Name == "PotionSickness" && item.healLife > 0 && Config.Server.EnableLife) {
					float delay = GetLifeSickness(item, player);
					tooltips[i].text = "Adds " + Math.Round(delay / 60f) + " seconds of potion sickness";
					if (DALib.Config.Global.Debug) {
						tooltips[i].text += " [c/"+DALib.Colors.DebugColor.Hex3()+":(VPS)]";
					}
				}
				if (tooltips[i].Name == "ManaSickness" && item.healMana > 0 && Config.Server.EnableLife) {
					float delay = GetManaSickness(item, player);
					tooltips[i].text = "Adds " + Math.Round(delay / 60f) + " seconds of mana sickness up to " + (Player.manaSickTimeMax / 60f) + " seconds";
					if (DALib.Config.Global.Debug) {
						tooltips[i].text += " [c/"+DALib.Colors.DebugColor.Hex3()+":(VPS)]";
					}
				}
			}
		}
	}
}
